#ifndef LISTEGEZICI_HPP
#define LISTEGEZICI_HPP

#include <dugum.hpp>

template <typename Nesne>

class ListeGezici{
  public:
    Dugum<Nesne> *simdiki;
    ListeGezici(){
      simdiki=NULL;
    }
    ListeGezici(Dugum<Nesne> *dugum){
      simdiki=dugum;
    }
    bool SonaGeldimi()const{
      return simdiki==NULL;
    }
    void ilerle(){
      //if(SonaGeldimi()) throw Hata
      simdiki=simdiki->ileri;
    }
    const Nesne& Getir()const{
      //if(SonaGeldimi()) throw Hata
      return simdiki->veri;
    }
};

#endif
